from abc import ABC, abstractmethod

class Person(ABC):
	@abstractmethod	
	def getFullName(self):
		pass
	def addRequest(self):
		pass
	def checkRequest(self):
		pass
	def addUser(self):
		pass

class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def get_firstName(self):
		print(f"First name of the employee is {self._firstName}")

	def get_lastName(self):
		print(f"First name of the employee is {self._lastName}")

	def get_email(self):
		print(f"Email of the employee is {self._email}")

	def get_department(self):	
		print(f"Department of the employee is {self._department}")

	def set_Name(self, name):
		self._name = name

	def set_Email(self, email):
		self._email = email

	def set_department(self):	
		self._department = department

	def getFullName(self):
		return(f"{self._firstName} {self._lastName}")

	def addRequest(self):
		return("Request has been added")

	def checkRequest(self):
		pass

	def addUser(self):
		pass

	def login(self):
		return(f"{self._email} has logged in")

	def logout(self):
		return(f"{self._email} has logged out")


class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department
		self.__members = []

	def get_Name(self):
		print(f"Name of the employee is {self._firstName} {self._lastName}")

	def get_Email(self):
		print(f"Email of the employee is {self._email}")

	def get_department(self):	
		print(f"Department of the employee is {self._department}")


	def set_Name(self, name):
		self._name = name

	def set_Email(self, email):
		self._email = email

	def set_department(self):	
		self._department = department

	def getFullName(self):
		return(f"{self._firstName} {self._lastName}")

	def addRequest(self):
		print("Request has been added")

	def checkRequest(self):
		pass

	def addUser(self):
		pass

	def login(self):
		print(f"{self._email} has logged in")

	def logout(self):
		print(f"{self._email} has logged out")

	def addMember(self, employee):
		# name = f'{employee.get_firstName} {employee.get_lastName}'
		self.__members.append(employee)

	def get_members(self):
		# print(len(self._members))
		return self.__members
		# print(f"{self._members}")
		# for x in range(len(self._members)):
		# 	print(f"{self._members}")


class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def get_Name(self):
		print(f"Name of the employee is {self._firstName} {self._lastName}")

	def get_Email(self):
		print(f"Email of the employee is {self.email}")

	def get_department(self):	
		print(f"Department of the employee is {self.email}")

	def set_Name(self, name):
		self._name = name

	def set_Email(self, email):
		self._email = email

	def set_department(self):	
		self._department = department

	def getFullName(self):
		return(f"{self._firstName} {self._lastName}")

	def addRequest(self):
		print("Request has been added")

	def checkRequest(self):
		pass

	def addUser(self):
		return("User has been added")

	def login(self):
		print(f"{self._email} has logged in")

	def logout(self):
		print(f"{self._email} has logged out")


class Request():
	def __init__(self, name, requester, dateRequested):
		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested
		self._status = "In process"

	def updateRequest(self):
		print(f"Request {self._name} has been updated")


	def closeRequest(self):
		return(f'Request {self._name} has been closed')


	def cancelRequest(self):
		print(f"Request {self._name} has been cancelled")

	def set_status(self, status):
		self._status = status

	def get_status(self):
		print(f"{self._status}")

	def get_name(self):
		print(f"{self._name}")

	def set_name(self, name):
		self._name = name


employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")


assert employee1.getFullName() == 'John Doe', "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"

assert employee2.addRequest() == "Request has been added"

assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
	print(indiv_emp.getFullName())
assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
